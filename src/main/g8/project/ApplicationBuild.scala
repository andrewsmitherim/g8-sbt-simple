import _root_.net.virtualvoid.sbt.graph.Plugin._
import sbt.Keys._
import sbt._
import sbtassembly.Plugin.AssemblyKeys._
import sbtassembly.Plugin._
import sbtbuildinfo.Plugin._
import sbtrelease.ReleasePlugin._
import java.util.regex.Pattern

object ApplicationBuild extends Build {

  lazy val commonResolvers = Seq(
    "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/",
    "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/"
  )

  lazy val coreDependencies: Seq[ModuleID] = Seq(
    "com.typesafe" % "config" % "1.2.0",
    "com.typesafe" %% "scalalogging-slf4j" % "1.1.0",
    "org.slf4j" % "jcl-over-slf4j" % "1.7.5",
    "ch.qos.logback" % "logback-classic" % "1.1.2"
  )

  lazy val testDependencies: Seq[ModuleID] = Seq(
    "org.specs2" %% "specs2" % "2.3.10" % "test",
    "org.scalacheck" %% "scalacheck" % "1.11.3" % "test"
  )

  lazy val buildSettings = Seq(
    organization := "$organization$",
    scalaVersion := "$scala_version$",
    resolvers ++= commonResolvers,
    scalacOptions ++= Seq("-unchecked", "-deprecation")
  )

  /*lazy val publishSettings = Seq(
    publishTo <<= version { version: String =>
      val artifactory = "http://tea-tools-artifactory01.tea.mindcandy.com:8081/artifactory/"
      val revisionProperty = if (!appVersion.isEmpty) ";revision=" + appVersion else ""
      val timestampProperty = ";build.timestamp=" + new java.util.Date().getTime
      val props = timestampProperty + revisionProperty
      if (version.trim.endsWith("SNAPSHOT")) Some("snapshots" at artifactory + "libs-snapshot-local" + props)
      else Some("releases" at artifactory + "libs-release-local" + props)
    }
  )*/

  lazy val appVersion: String = Option(System.getenv("BUILD_VCS_NUMBER")) match {
    case Some(version) => version
    case _ =>
      val pattern = Pattern.compile("\"(.+)\"")
      val matcher = pattern.matcher(IO.read(file("./version.sbt")))
      if (matcher.find) matcher.group(1) else ""
  }

  lazy val $name;format="normalize"$Settings =
    Defaults.defaultSettings ++
      buildSettings ++
      assemblySettings ++
      buildInfoSettings ++
      releaseSettings
      //++ publishSettings

  lazy val appBuildInfo = Seq(
    sourceGenerators in Compile <+= buildInfo,
    buildInfoKeys := Seq[BuildInfoKey](name, scalaVersion, sbtVersion),
    buildInfoPackage := "$organization$",
    buildInfoKeys ++= Seq[BuildInfoKey](
      "version" -> appVersion
    )
  )

  lazy val $name;format="normalize"$ = Project(
    id = "$name;format="normalize"$",
    base = file("."),
    settings = $name;format="normalize"$Settings ++ 
      appBuildInfo ++ Seq(
      name := "$name;format="normalize"$",
      libraryDependencies ++= coreDependencies,
      libraryDependencies ++= testDependencies,
      fork in run := false,
      fork in test := true,
      fork in testOnly := true,
      javaOptions in Test += "-XX:MaxPermSize=128m",
      jarName in assembly <<= (crossTarget, version).map((target, versionString) => "$name;format="normalize"$-%s.jar".format(versionString)),
      mainClass in(Compile, run) := Some("$organization$.$name;format="normalize"$.$name;format="Camel"$App"),
      mainClass in assembly := Some("$organization$.$name;format="normalize"$.$name;format="Camel"$App"),
      publishArtifact in(Compile, packageBin) := false,
      artifact in(Compile, assembly) ~= { (art: Artifact) => art}
    )
  ).settings(graphSettings: _*)
}