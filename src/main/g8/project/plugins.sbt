addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "$sbt_assembly_version$")

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "$sbt_idea_version$")

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "$sbt_dependency_graph_version$")

addSbtPlugin("com.github.gseitz" % "sbt-release" % "$sbt_release_version$")

addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "$sbt_buildinfo_version$")
