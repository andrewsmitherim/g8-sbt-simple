package $organization$.$name;format="normalize"$

import org.specs2.{Specification, ScalaCheck}

case class $name;format="Camel"$AppSpec() extends Specification with ScalaCheck {

  System.setProperty("config.file", "./local.config")

  def is = s2"""
      $name;format="Camel"$App
        returns the correct version \$getVersion
        returns the correct config \$getConfig
    """

  def getVersion = $name;format="Camel"$App().version must be_==("0.0.1-SNAPSHOT")
  def getConfig = $name;format="Camel"$App().config must be_==("123")
}