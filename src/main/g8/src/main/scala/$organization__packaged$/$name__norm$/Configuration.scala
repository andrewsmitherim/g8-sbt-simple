package $organization$.$name;format="normalize"$

import com.typesafe.config.Config

trait LogicConfigComponent {
    def logicConfig: LogicConfig
}

class LogicConfig(config: Config) {
    val test = config.getString("$name;format="normalize"$.test")
}

trait Configuration {
  def logicConfig: LogicConfig
}

trait ConfigurationComponent extends Configuration {
  require(Option(System.getProperty("config.file")).ensuring(_.isDefined, "Missing property 'config.file'. Try running with '-Dconfig.file=./local.config'.").isDefined)
  private[this] val config = com.typesafe.config.ConfigFactory.load()
  override val logicConfig = new LogicConfig(config)
}