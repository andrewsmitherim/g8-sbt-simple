package $organization$.$name;format="normalize"$

import $organization$.BuildInfo
import com.typesafe.scalalogging.slf4j.StrictLogging

case class $name;format="Camel"$App() extends ConfigurationComponent {
  val version = BuildInfo.version
  val config = logicConfig.test
}

object $name;format="Camel"$App extends App with StrictLogging {
  val application = new $name;format="Camel"$App
  val version = application.version
  val config = application.config

  logger.info(s"version:\$version config:\$config")
}