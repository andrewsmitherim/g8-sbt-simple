# $name$

## Huh!?
$name$ is a simple [SBT](https://github.com/sbt/sbt) application.

## Configuration
Edit `local.config` as required for the environment, then simply run the app in SBT.
Run other environment configuration by specifying the relevant file.

## Run
`sbt -Dconfig.file=./local.config run`