# Description
g8-sbt-simple is a [giter8](https://github.com/n8han/giter8) template for generating simple [SBT](https://github.com/sbt/sbt) applications.

# Requirements
Install [SBT](https://github.com/sbt/sbt) and [giter8](https://github.com/n8han/giter8).
I originally used brew to install giter8 however the version is a little old and never worked when using the template generation from the repo.

# Usage
Run the template generation

`g8 https://andrewsmitherim@bitbucket.org/andrewsmitherim/g8-sbt-simple.git`

Set the appropriate properties
```shell
	name							=> Project name (no spaces)
	organization					=> Project organisation
	version							=> Project version to begin with
	description 					=> Project description
	scala_version					=> Version of Scala
	sbt_version						=> Version of SBT
	sbt_dependency_graph_version	=> Version of SBT Dependency Graph plugin
	sbt_assembly_version			=> Version of SBT Assembly plugin
	sbt_idea_version				=> Version of SBT Idea plugin
	sbt_release_version				=> Version of SBT Release plugin
	sbt_buildinfo_version			=> Version of SBT BuildInfo plugin
	verbatim						=> Files to be ignored
```

Once completed you can go into your new project and run it:

`sbt -Dconfig.file=./local.config run`
